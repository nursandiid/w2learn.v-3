<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Post;
use App\Tag;

class SearchController extends Controller
{
    public function search(Request $request)
    {
		$query       = $request->input('query');
		$posts       = Post::where('title','LIKE',"%$query%")->with('categories')->approved()->published()->latest()->paginate(8);

		$posts_count = Post::where('title','LIKE',"%$query%")->with('categories')->approved()->published()->count();
        
        return view('new-search', compact('posts','query', 'posts_count'));
    }

    public function getData(Request $request)
    {
    	$query       = $request->input('query');
		$posts       = Post::select('slug', 'title')->where('title','LIKE',"%$query%")->approved()->published()->latest()->take(10)->get();
		$output = '';

		if ($posts->count() == 0) {
			$output .= '<a id="sorry" style="cursor: not-allowed;">Sorry, the keyword you entered was not found</a>';
		}

		foreach ($posts as $post) {
			$output .= '<a href="'. url('/post/'. $post->slug) .'">'. $post->title .'</a>';
		}

        return array('output' => $output);
    }
}
