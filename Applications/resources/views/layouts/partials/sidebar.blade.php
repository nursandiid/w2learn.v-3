<aside class="main-sidebar sidebar-light-indigo elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link navbar-indigo text-white">
        <img src="{{ asset('/img/icon.png') }}" alt="W2learn Logo" class="brand-image img-circle elevation-3">
        <span class="brand-text font-weight-light">W2Learn CMS</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ Storage::disk('public')->url('profile/'. Auth::user()->image) }}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="{{ route('admin.setting') }}" class="d-block">{{ Auth::user()->name }}</a>
            </div>
        </div>
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                @if(Request::is('admin*'))
                
                <li class="nav-item">
                    <a href="{{ route('admin.dashboard') }}" class="nav-link {{ Request::is('admin/dashboard') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-home"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li class="nav-header">MAIN NAVIGATION</li>
                <li class="nav-item">
                    <a href="{{ route('admin.tag.index') }}" class="nav-link {{ Request::is('admin/tag*') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-tag"></i>
                        <p>Tags</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.category.index') }}" class="nav-link {{ Request::is('admin/category*') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-th-list"></i>
                        <p>Categories</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.post.index') }}" class="nav-link {{ Request::is('admin/post*') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-book"></i>
                        <p>Posts</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.post.pending') }}" class="nav-link {{ Request::is('admin/pending/post') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-book"></i>
                        <p>Pending Posts</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.favorite.index') }}" class="nav-link {{ Request::is('admin/favorite') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-heart"></i>
                        <p>Favorite Posts</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.comment.index') }}" class="nav-link {{ Request::is('admin/comments') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-comments"></i>
                        <p>Comments</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.author.index') }}" class="nav-link {{ Request::is('admin/authors') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-user-circle"></i>
                        <p>Authors</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.subscriber.index') }}" class="nav-link {{ Request::is('admin/subscriber') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-users"></i>
                        <p>Subscribers</p>
                    </a>
                </li>
                <li class="nav-header">SYSTEM</li>

                <li class="nav-item">
                    <a href="{{ route('admin.setting') }}" class="nav-link {{ Request::is('admin/setting/general') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-cogs"></i>
                        <p>Setting</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('logout') }}"
                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <i class="nav-icon fas fa-sign-out-alt"></i>
                        <p>Sign Out</p>
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
                @endif
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>