<!-- Main Footer -->
<footer class="main-footer">
	<!-- To the right -->
	<div class="float-right d-none d-sm-inline">
		Created with <i class="fas fa-heart text-danger" style="font-size: .8em"></i> by Nursandi
	</div>
	<!-- Default to the left -->
	<strong>Copyright &copy; 2017-2019 <a href="https://w2learn.com">W2Learn.com</a>.</strong> All rights reserved.
</footer>