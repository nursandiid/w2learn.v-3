<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    
    <title>@yield('title') . W2learn</title>
    <link rel="icon" href="{{ asset('/img/icon.png') }}">
    
    <!-- Icons -->
    <link rel="stylesheet" href="{{ asset('/AdminLTE/plugins/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/4.5.6/css/ionicons.min.css">
    
    <!-- Plugins -->
    <link rel="stylesheet" href="{{ asset('/AdminLTE/plugins/lightbox/css/lightbox.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.10/styles/default.min.css">
    <link rel="stylesheet" href="{{ asset('/css/hljs-custom-by-laracasts.css') }}">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('/AdminLTE/dist/css/adminlte.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/custom.css') }}">

    <!-- Google Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Rajdhani:600,700|Roboto:400,500,500i">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/tonsky/FiraCode@1.207/distr/fira_code.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Mono&display=swap" rel="stylesheet">
    <style>
        p img {
            margin: auto;
        }

        pre {
            padding: 0;
            border-radius: 5px;
        }

        .result {
            margin-top: 1em;
            margin-bottom: 1em;
            border: 1px solid #ddd;
            border-radius: 5px;
            padding: .5em;
            background: #f8f9fa;
        }

        .layout-navbar-fixed .wrapper .brand-link {
            width: 250px;
        }

        .layout-navbar-fixed.sidebar-collapse .wrapper .brand-link {
            height: calc(3.5rem + 1px);
            width: 250px;
        }

        .sidebar-light-indigo .nav-sidebar > .nav-item > .nav-link {
            color: #000;
            border-left: 3px solid #fff;
        }
        
        .sidebar-light-indigo .nav-sidebar > .nav-item > .nav-link.active {
            background-color: #fff;
            color: #000;
            border-left: 3px solid #6610f2;
        }

    </style>

    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script> 
    <script> (adsbygoogle = window.adsbygoogle || []).push({ google_ad_client: "ca-pub-9070824081326686", enable_page_level_ads: true }); </script>
    @stack('css')
</head>

<body class="hold-transition sidebar-collapse layout-fixed layout-navbar-fixed">
    <div class="wrapper">
        
        @include('layouts.frontend.partials.nav')
        
        @yield('banner')

        <div class="content-wrapper">
            <div class="content">
                
                @yield('main-content')

            </div>
        </div>
        
        @include('layouts.frontend.partials.sidebar')
        @include('layouts.frontend.partials.footer')
    </div>

    <!-- REQUIRED SCRIPTS -->
    <!-- jQuery -->
    <script src="{{ asset('/AdminLTE/plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('/AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- Plugins -->
    <script src="{{ asset('/AdminLTE/plugins/lightbox/js/lightbox.min.js') }}"></script>
    <script src="{{ asset('/js/owl.carousel.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.10/highlight.min.js"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('/AdminLTE/dist/js/adminlte.min.js') }}"></script>

    <script>
        $(document).ready(function () {
            hljs.initHighlightingOnLoad()
            let card = $('#card-search').hide()

            $('html, body').click(function () {
                card.hide()
            })

            $('#navbar-search').on('keyup blur change', function (e) {
                let list = $('#search-content');

                if ($(this).val() !== '') {
                    $.ajax({
                        url: '{{ url('search/getdata') }}',
                        type: 'get',
                        data: $(this).parent().parent().serialize(),
                        success: function (data) {
                            card.show()
                            list.html(data.output)
                        }
                    })
                }

                if (e.keyCode == 27 || $(this).val().length < 1) {
                    card.hide()
                    list.html('')
                }
            })

            $('.nav-sidebar a').filter(function() {
                return this.href == window.location.href;
                console.log(this.href)
            }).addClass('active');

            $('.nav-sidebar a').filter(function() {
                return this.href +'#' == window.location.href;
            }).addClass('active');
        })
    </script>
    @stack('scripts')
</body>
</html>