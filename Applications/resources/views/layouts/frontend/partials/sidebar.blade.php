<!-- Main Sidebar -->
<aside class="main-sidebar sidebar-light-indigo">
    <!-- Brand Logo -->
    <a href="{{ url('/') }}" class="brand-link navbar-indigo d-none d-lg-none" style="background-color: #4a01c1;">
        <img src="{{ asset('/img/logo2.png') }}" alt="W2learn Logo" class="brand-image">
    </a>

    <!-- Sidebar -->
    <div class="sidebar" style="margin-top: 0;">
        
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="{{ url('/') }}" class="nav-link {{ Request::is('/') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-home"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('/articles?page=2') }}" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>Artikel</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('/daftar-isi') }}" class="nav-link">
                        <i class="nav-icon fas fa-server"></i>
                        <p>Daftar Isi</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('/templates') }}" class="nav-link">
                        <i class="nav-icon fas fa-images"></i>
                        <p>Template</p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="{{ url('/about') }}" class="nav-link">
                        <i class="nav-icon far fa-question-circle"></i>
                        <p>
                            Informasi
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ url('/about') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Tentang Kami</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/services') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Ketentuan</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/privacy') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Kebijakan</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="{{ url('/login') }}" class="nav-link">
                        <i class="nav-icon fas fa-sign-in-alt"></i>
                        <p>Masuk</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('/register') }}" class="nav-link">
                        <i class="nav-icon fas fa-terminal"></i>
                        <p>Daftar</p>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</aside>