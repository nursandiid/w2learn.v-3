@extends('layouts.master')

@section('title', 'Produk')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active">Produk</li>
@endsection

@section('main-content')
<div class="row">

    <div class="col-md-12">
        @card
            @slot('title')
                <h5 class="card-title">List Produk</h5>
                <div class="card-tools">
                    <a href="{{ url('/products/create') }}" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i> Tambah</a>
                </div>
            @endslot

            @table
                @slot('thead')
                    <th class="text-center">#</th>
                    <th>Nama</th>
                    <th>Stok</th>
                    <th>Harga</th>
                    <th>Kategori</th>
                    <th>Last Update</th>
                    <th width="10%">Aksi</th>
                @endslot

                @foreach ($products as $product)
                <tr>
                    <td width="10%" class="text-center">
                        <img src="{{ Storage::disk('public')->url('uploads/products/'. $product->photo) }}" alt="{{ $product->photo }}" width="80" class="rounded border">
                    </td>
                    <td> <sup class="badge badge-primary">{{ $product->code }}</sup> {{ $product->name }}</td>
                    <td>{{ $product->stock }}</td>
                    <td>{{ number_format($product->price) }}</td>
                    <td>{{ $product->category->name }}</td>
                    <td>{{ $product->created_at->toFormattedDateString() }}</td>
                    <td>
                        <a href="{{ route('products.edit', $product->id) }}" class="btn btn-primary btn-sm float-left"><i class="fa fa-edit"></i></a>
                        <form action="{{ route('products.destroy', $product->id) }}" method="post">
                            @csrf @method('delete')
                            <button class="btn btn-danger btn-sm ml-2" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i></button>
                        </form>
                    </td>
                </tr>
                @endforeach
            @endtable
            
            @if($products->count() >= 10)
                <div class="mt-3 float-right">
                    {{ $products->links() }}
                </div>                
            @endif
        @endcard
    </div>
</div>
@include('components.success')
@endsection