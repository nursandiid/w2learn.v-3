@extends('layouts.frontend.master')

@section('title', 'Daftar Isi')

@section('banner')
<div class="container-fluid single-intro">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <h3 class="font-weight-light mb-4">All Categories :</h3>
            @foreach ($categories as $category)
                <span class="tags tag-{{ $category->slug }}"><a href="{{ url('/category/'. $category->slug) }}">{{ $category->name }}</a></span>
            @endforeach
        </div>
    </div>
</div>
@endsection

@section('main-content')
<div class="container-fluid">
    <div class="row pt-5">
        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    <h1 class="text-center text-capitalize header-title my-3">Daftar Isi</h1>
                    <!-- second ads -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-9070824081326686"
                         data-ad-slot="9828947544"
                         data-ad-format="auto"
                         data-full-width-responsive="true"></ins>
                    <script>
                         (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                    
                    <ul type="circle" class="toc">
                        @foreach ($tocs as $post)
                            <li>
                                <a href="{{ url('/post/'. $post->slug) }}" title="{{ $post->title }}" target="_blank">{{ $post->title }}
                                </a>
                            </li>
                        @endforeach
                   </ul>
                </div>
            </div>
        </div>
        <div class="col-md-3" id="widget-col3">
            @include('layouts.frontend.partials.widget')            
        </div>
    </div>
</div>
@endsection

@push('scripts')
	<script>
		$('.content-wrapper').css('marginTop', '0')
	</script>
@endpush