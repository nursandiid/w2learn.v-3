@extends('layouts.master')

@section('title','Tags')
@section('breadcrumb')
    @parent
    <li class="breadcrumb-item">Tags</li>
@endsection

@section('main-content')
    @card
        @slot('title')
            <a href="{{ route('admin.tag.create') }}" class="btn btn-primary btn-sm"><i class="fas fa-plus"></i> Add</a>
        @endslot
        
        @table
            @slot('thead')
                <th>ID</th>
                <th>Name</th>
                <th>Post Count</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th>Action</th>
            @endslot

            @foreach($tags as $key => $tag)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $tag->name }}</td>
                    <td>{{ $tag->posts->count() }}</td>
                    <td>{{ $tag->created_at->toDateString() }}</td>
                    <td>{{ $tag->updated_at->toDateString() }}</td>
                    <td class="text-center">
                        <form action="{{ route('admin.tag.destroy', $tag->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <a href="{{ route('admin.tag.edit',$tag->id) }}" class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></a>
                            <button class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')"><i class="fas fa-trash"></i></button>
                        </form>
                    </td>
                </tr>
            @endforeach
        @endtable
        @slot('footer')
            
        @endslot
    @endcard
@endsection

@push('scripts')
    <script>
        $('.table').DataTable({
            "language": {
              "emptyTable": "Ups data masih kosong!.",
              "zeroRecords": "Ups data tidak ditemukan!."
            }
        })

        $('.card-footer').hide()
    </script>
@endpush