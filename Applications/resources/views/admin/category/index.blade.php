@extends('layouts.master')

@section('title','Categories')
@section('breadcrumb')
    @parent
    <li class="breadcrumb-item">Categories</li>
@endsection

@section('main-content')
    @card
        @slot('title')
            <a href="{{ route('admin.category.create') }}" class="btn btn-primary btn-sm"><i class="fas fa-plus"></i> Add</a>
        @endslot
        
        @table
            @slot('thead')
                <th>ID</th>
                <th>Name</th>
                <th>Post Count</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th>Action</th>
            @endslot
                                
            @foreach($categories as $key => $category)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $category->name }}</td>
                    <td>{{ $category->posts->count() }}</td>
                    <td>{{ $category->created_at->toDateString() }}</td>
                    <td>{{ $category->updated_at->toDateString() }}</td>
                    <td class="text-center">
                        <form action="{{ route('admin.category.destroy', $category->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <a href="{{ route('admin.category.edit',$category->id) }}" class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></a>
                            <button class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')"><i class="fas fa-trash"></i></button>
                        </form>
                    </td>
                </tr>
            @endforeach
        @endtable

        @slot('footer')
            
        @endslot
    @endcard
@endsection

@push('scripts')
    <script>
        $('.table').DataTable({
            "language": {
              "emptyTable": "Ups data masih kosong!.",
              "zeroRecords": "Ups data tidak ditemukan!."
            }
        })

        $('.card-footer').hide()
    </script>
@endpush