@extends('layouts.master')

@section('title','Post')

@push('css')
    <link rel="stylesheet" href="{{ asset('/AdminLTE/plugins/summernote/summernote-bs4.css') }}">
@endpush

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item"><a href="{{ route('admin.post.index') }}">Post</a></li>
    <li class="breadcrumb-item">Add</li>
@endsection

@section('main-content')
<form action="{{ route('admin.post.update',$post->id) }}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    
    <div class="row">
        <div class="col-md-8">
            @card
                @slot('title')
                    <h5 class="card-title">Add Post</h5>
                @endslot

                @csrf
                <div class="form-group">
                    <label class="form-label">Post Title</label>
                    <input type="text" id="title" class="form-control" name="title" value="{{ $post->title }}">
                </div>

                <div class="custom-file mt-2">
                    <input type="file" class="custom-file-input" id="customFile" name="image">
                    <label class="custom-file-label" for="customFile">Choose file</label>
                </div>

                <div class="custom-control custom-checkbox mt-3">
                    <input type="checkbox" class="custom-control-input" id="customCheck1" name="status" value="1" {{ $post->status == true ? 'checked' : '' }}>
                    <label class="custom-control-label" for="customCheck1">Publish</label>
                </div>
                
                @slot('footer')
                    
                @endslot
            @endcard
        </div>

        <div class="col-md-4">
            @card
                @slot('title')
                    <h5 class="card-title">Categories and Tags</h5>
                @endslot
                
                <div class="form-group">
                    <label for="category">Select Category</label>
                    <select name="categories[]" id="category" class="form-control" multiple>
                        @foreach($categories as $category)
                            <option
                                @foreach($post->categories as $postCategory)
                                    {{ $postCategory->id == $category->id ? 'selected' : '' }}
                                @endforeach
                                value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="tag">Select Tags</label>
                    <select name="tags[]" id="tag" class="form-control" multiple>
                       @foreach($tags as $tag)
                            <option
                                @foreach($post->tags as $postTag)
                                    {{ $postTag->id == $tag->id ? 'selected' :'' }}
                                @endforeach
                                value="{{ $tag->id }}">{{ $tag->name }}</option>
                        @endforeach
                    </select>
                </div>            
                
                @slot('footer')
                    
                @endslot
            @endcard
        </div>
            
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Body</h5>
                </div>
                
                <div class="card-body">
                    <textarea name="body" class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ $post->body }}</textarea>
                </div>

                <div class="card-footer save">
                    <a class="btn btn-danger" href="{{ route('admin.post.index') }}">Back</a>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@push('scripts')
    <script src="{{ asset('AdminLTE/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <script>

        $(function () {
            $('.textarea').summernote()

            $('select').select2()
            $('.card-footer:not(.save)').hide()

            $('[type=checkbox]').prop('indeterminate', true)
        })
    </script>
@endpush