@extends('layouts.master')

@section('title', $post->title)
@section('breadcrumb')
    @parent
    <li class="breadcrumb-item">Post</li>
    <li class="breadcrumb-item">Show</li>
@endsection

@section('main-content')
    <div class="row">
        <div class="col-lg-8 col-12">
            @card
                @slot('title')
                    <a href="{{ route('admin.post.index') }}" class="btn btn-danger btn-sm">Back</a>

                    <div class="card-tools">
                          @if($post->is_approved == false)
                            <button type="button" class="btn btn-success waves-effect pull-right" onclick="$('#approval-form').submit()">
                                <i class="fas fa-check"></i>
                                <span>Approve</span>
                            </button>
                            <form method="post" action="{{ route('admin.post.approve', $post->id) }}" id="approval-form" style="display: none">
                                @csrf
                                @method('PUT')
                            </form>
                        
                        @else
                            <button type="button" class="btn btn-success btn-sm" disabled style="cursor: not-allowed;">
                                <i class="fas fa-check"></i>
                                <span>Approved</span>
                            </button>
                        @endif
                    </div>
                @endslot

                <h3>{{ $post->title }}</h3>
                <h5 class="card-title mb-3">
                    Posted By <strong> <a href="">{{ $post->user->name }}</a></strong> on {{ $post->created_at->toFormattedDateString() }}
                </h5>

                {!! $post->body !!}
                
                @slot('footer')
                    
                @endslot
            @endcard
        </div>
        <div class="col-lg-4 col-12">
            @card
                @slot('title')
                    <h5 class="card-title">Categories</h5>
                @endslot

                @foreach($post->categories as $category)
                    <span class="label bg-danger p-1">{{ $category->name }}</span>
                @endforeach

                @slot('footer')
                    
                @endslot
            @endcard

            @card
                @slot('title')
                    <h5 class="card-title">Tags</h5>
                @endslot

                @foreach($post->tags as $tag)
                    <span class="label bg-indigo p-1">{{ $tag->name }}</span>
                @endforeach

                @slot('footer')
                    
                @endslot
            @endcard
            
            @card
                @slot('title')
                    <h5 class="card-title">Featured Image</h5>
                @endslot
                
                <img class="img-fluid" src="{{ Storage::disk('public')->url('post/'. $post->image) }}" alt="">

                @slot('footer')
                    
                @endslot
            @endcard
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $('.card-footer').addClass('d-none d-lg-none')
    </script>
@endpush