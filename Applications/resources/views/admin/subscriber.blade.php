@extends('layouts.master')

@section('title','Subscribers')
@section('breadcrumb')
    @parent
    <li class="breadcrumb-item">Subscribers</li>
@endsection

@section('main-content')
    @card
        @slot('title')
            <h5 class="card-title">All Subscribers</h5>
        @endslot

        @table
            @slot('thead')
                <th>ID</th>
                <th>Email</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th>Action</th>
            @endslot

            @foreach($subscribers as $key => $subscriber)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $subscriber->email }}</td>
                    <td>{{ $subscriber->created_at->toDateString() }}</td>
                    <td>{{ $subscriber->updated_at->toDateString() }}</td>
                    <td class="text-center">
                        <button class="btn btn-danger btn-sm" type="button" onclick="$('#delete-form').submit()">
                            <i class="fas fa-trash"></i>
                        </button>
                        <form id="delete-form" action="{{ route('admin.subscriber.destroy',$subscriber->id) }}" method="POST" style="display: none;">
                            @csrf
                            @method('DELETE')
                        </form>
                    </td>
                </tr>
            @endforeach            
        @endtable

        @slot('footer')
            
        @endslot
    @endcard
@endsection

@push('scripts')
    <script>
        $('.table').DataTable({
            "language": {
              "emptyTable": "Ups data masih kosong!.",
              "zeroRecords": "Ups data tidak ditemukan!."
            }
        })

        $('.card-footer').hide()
    </script>
@endpush