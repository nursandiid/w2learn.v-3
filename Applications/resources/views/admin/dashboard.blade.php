@extends('layouts.master')

@section('title','Dashboard')
@push('css')
    <!-- fullCalendar -->
    <link rel="stylesheet" href="{{ asset('/AdminLTE/plugins/fullcalendar/main.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/AdminLTE/plugins/fullcalendar-daygrid/main.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/AdminLTE/plugins/fullcalendar-timegrid/main.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/AdminLTE/plugins/fullcalendar-bootstrap/main.min.css') }}">
@endpush

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item">Dashboard</li>
@endsection

@section('main-content')
<div class="row">

    <div class="col-6 col-md-3">
        <div class="small-box bg-primary">
            <div class="inner">
                <h3>{{ $posts->count() }}</h3>
                <p>Total Posts</p>
            </div>
            <div class="icon">
                <i class="fas fa-book"></i>
            </div>
        </div>
    </div>
    <div class="col-6 col-md-3">
        <div class="small-box bg-danger">
            <div class="inner">
                <h3>{{ Auth::user() ->favorite_posts()->count() }}</h3>
                <p>Total Favorite</p>
            </div>
            <div class="icon">
                <i class="fas fa-heart"></i>
            </div>
        </div>
    </div>
    <div class="col-6 col-md-3">
        <div class="small-box bg-indigo">
            <div class="inner">
                <h3>{{ $total_pending_posts }}</h3>
                <p>Pending Posts</p>
            </div>
            <div class="icon">
                <i class="fas fa-book"></i>
            </div>
        </div>
    </div>
    <div class="col-6 col-md-3">
        <div class="small-box bg-success">
            <div class="inner">
                <h3>{{ $all_views }}</h3>
                <p>Total Views</p>
            </div>
            <div class="icon">
                <i class="fas fa-eye"></i>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-12 col-sm-12 col-md-4 col-lg-3">
        <div class="row">
            <div class="col-md-12 col-sm-6 col-6">
                <div class="small-box bg-orange">
                    <div class="inner">
                        <h3>{{ $category_count }}</h3>
                        <p>Categories</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-th-list"></i>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-sm-6 col-6">
                <div class="small-box bg-gray">
                    <div class="inner">
                        <h3>{{ $tag_count }}</h3>
                        <p>Tags</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-tag"></i>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-sm-6 col-6">
                <div class="small-box bg-gray-dark">
                    <div class="inner">
                        <h3>{{ $author_count }}</h3>
                        <p>Total Author</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-user-circle"></i>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-sm-6 col-6">
                <div class="small-box bg-indigo">
                    <div class="inner">
                        <h3>{{ $new_authors_today }}</h3>
                        <p>Today Author</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-user-circle"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-9">
        <div class="card card-indigo card-outline">
            <div class="card-header border-bottom-0">
                <h5 class="card-title text-center">Most Popular Post</h5>
            </div>
            <div class="card-body p-0">
                <div class="table-responsive">
                    <table class="table table-striped table-sm">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Author</th>
                                <th width="3%">Views</th>
                                <th width="3%">Favorite</th>
                                <th width="3%">Comments</th>
                                <th width="3%">Status</th>
                                <th width="3%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($popular_posts as $key=>$post)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td title="{{ $post->title }}">{{ str_limit($post->title, '40') }}</td>
                                    <td>{{ $post->user->name }}</td>
                                    <td>{{ $post->view_count }}</td>
                                    <td>{{ $post->favorite_to_users_count }}</td>
                                    <td>{{ $post->comments_count }}</td>
                                    <td class="text-center">
                                        @if($post->status == true)
                                            <span class="badge badge-success">Published</span>
                                        @else
                                            <span class="badge badge-danger">Pending</span>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-primary btn-sm" target="_blank" href="{{ route('post.detail',$post->slug) }}"><i class="fas fa-eye"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12 col-lg-12">
        <div class="card">
            <div class="card-body padding-0 pt-3">
                <h5 class="card-title text-center">Chart Statistics</h5>
                <div class="chart">
                    <canvas id="barChart" style="height:230px; min-height:230px"></canvas>
                </div>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-sm-4 col-lg-4 col-6">
                        <div class="description-block border-right">
                            <span class="description-percentage text-success"><i class="fas fa-caret-up"></i> 17%</span>
                            <h5 class="description-header">35,210.43</h5>
                            <span class="description-text">TOTAL AUTHORS</span>
                        </div>
                    </div>
                    <div class="col-sm-4 col-lg-4 col-6">
                        <div class="description-block border-right">
                            <span class="description-percentage text-warning"><i class="fas fa-caret-left"></i> 0%</span>
                            <h5 class="description-header">10,390.90</h5>
                            <span class="description-text">TOTAL VISITORS</span>
                        </div>
                    </div>
                    <div class="col-sm-4 col-lg-4 col-6">
                        <div class="description-block">
                            <span class="description-percentage text-danger"><i class="fas fa-caret-down"></i> 20%</span>
                            <h5 class="description-header">24,813.53</h5>
                            <span class="description-text">TOTAL SUBSCRIBERS</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-4">
        <div class="card">
            <div class="card-header border-bottom-0">
                <h5 class="card-title text-center">Top 10 Active Author</h5>
            </div>
            <div class="card-body p-0">
                <div class="table-responsive">
                    <table class="table table-sm table-striped">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th class="text-center">Posts</th>
                            <th class="text-center">Comments</th>
                            <th class="text-center">Favorite</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($active_authors as $key=>$author)
                                <tr>
                                    <td>{{ $author->name }}</td>
                                    <td class="text-center">{{ $author->posts_count }}</td>
                                    <td class="text-center">{{ $author->comments_count }}</td>
                                    <td class="text-center">{{ $author->favorite_posts_count }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-8">
        <div class="sticky-top">
            <div class="card d-none d-lg-none">
                <div class="card-body">
                    <div id="external-events">
                        
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-body p-0">
                <div id="calendar"></div>
            </div>
        </div>
    </div>
</div>
@endsection


@push('scripts')
    <!-- CharatJS -->
    <script src="{{ asset('/AdminLTE') }}/plugins/chart.js/Chart.min.js"></script>

    <!-- Fullcalendar -->
    <script src="{{ asset('/AdminLTE') }}/plugins/fullcalendar/main.min.js"></script>
    <script src="{{ asset('/AdminLTE') }}/plugins/fullcalendar-daygrid/main.min.js"></script>
    <script src="{{ asset('/AdminLTE') }}/plugins/fullcalendar-timegrid/main.min.js"></script>
    <script src="{{ asset('/AdminLTE') }}/plugins/fullcalendar-interaction/main.min.js"></script>
    <script src="{{ asset('/AdminLTE') }}/plugins/fullcalendar-bootstrap/main.min.js"></script>
    
    <script>
        
    let areaChartData = {
        labels  : ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
        datasets: [
            {
                label               : 'Visitors',
                backgroundColor     : '#007bff',
                borderColor         : '#007bff',
                pointRadius          : false,
                pointColor          : '#3b8bba',
                pointStrokeColor    : 'rgba(60,141,188,1)',
                pointHighlightFill  : '#fff',
                pointHighlightStroke: 'rgba(60,141,188,1)',
                data                : [28, 48, 40, 19, 86, 27, 90]
            },
            {
                label               : 'Authors',
                backgroundColor     : '#28a745',
                borderColor         : '#28a745',
                pointRadius          : false,
                pointColor          : '#3b8bba',
                pointStrokeColor    : 'rgba(60,141,188,1)',
                pointHighlightFill  : '#fff',
                pointHighlightStroke: 'rgba(60,141,188,1)',
                data                : [28, 48, 40, 19, 86, 27, 90]
            },
            {
                label               : 'Subsribers',
                backgroundColor     : '#dc3545',
                borderColor         : '#dc3545',
                pointRadius         : false,
                pointColor          : 'rgba(210, 214, 222, 1)',
                pointStrokeColor    : '#c1c7d1',
                pointHighlightFill  : '#fff',
                pointHighlightStroke: 'rgba(220,220,220,1)',
                data                : [65, 59, 80, 81, 56, 55, 40]
            },
        ]
    }

    let barChartCanvas = $('#barChart').get(0).getContext('2d')
    let barChartData = jQuery.extend(true, {}, areaChartData)
    let temp0 = areaChartData.datasets[0]
    let temp1 = areaChartData.datasets[1]
    barChartData.datasets[0] = temp1
    barChartData.datasets[1] = temp0

    let barChartOptions = {
        responsive              : true,
        maintainAspectRatio     : false,
        datasetFill             : false
    }

    let barChart = new Chart(barChartCanvas, {
        type: 'bar', 
        data: barChartData,
        options: barChartOptions
    })

    $(function () {
        
        calendar()

        /* initialize the external events
         -----------------------------------------------------------------*/
        function ini_events(ele) {
          ele.each(function () {

            // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
            // it doesn't need to have a start or end
            var eventObject = {
              title: $.trim($(this).text()) // use the element's text as the event title
            }

            // store the Event Object in the DOM element so we can get to it later
            $(this).data('eventObject', eventObject)

            // make the event draggable using jQuery UI
            $(this).draggable({
              zIndex        : 1070,
              revert        : true, // will cause the event to go back to its
              revertDuration: 0  //  original position after the drag
            })

          })
        }

        function calendar() {
            ini_events($('#external-events div.external-event'))

            /* initialize the calendar
             -----------------------------------------------------------------*/
            //Date for the calendar events (dummy data)
            var date = new Date()
            var d    = date.getDate(),
                m    = date.getMonth(),
                y    = date.getFullYear()

            var Calendar = FullCalendar.Calendar;
            var Draggable = FullCalendarInteraction.Draggable;

            var containerEl = document.getElementById('external-events');
            var checkbox = document.getElementById('drop-remove');
            var calendarEl = document.getElementById('calendar');

            // initialize the external events
            // -----------------------------------------------------------------

            new Draggable(containerEl, {
              itemSelector: '.external-event',
              eventData: function(eventEl) {
                console.log(eventEl);
                return {
                  title: eventEl.innerText,
                  backgroundColor: window.getComputedStyle( eventEl ,null).getPropertyValue('background-color'),
                  borderColor: window.getComputedStyle( eventEl ,null).getPropertyValue('background-color'),
                  textColor: window.getComputedStyle( eventEl ,null).getPropertyValue('color'),
                };
              }
            });

            var calendar = new Calendar(calendarEl, {
              plugins: [ 'bootstrap', 'interaction', 'dayGrid', 'timeGrid' ],
              header    : {
                left  : 'prev,next today',
                center: 'title',
                right : 'dayGridMonth,timeGridWeek,timeGridDay'
              },
              events    : [
                {
          title          : 'All Day Event',
          start          : new Date(y, m, 1),
          backgroundColor: '#f56954', //red
          borderColor    : '#f56954' //red
        },
        {
          title          : 'Long Event',
          start          : new Date(y, m, d - 5),
          end            : new Date(y, m, d - 2),
          backgroundColor: '#f39c12', //yellow
          borderColor    : '#f39c12' //yellow
        },
        {
          title          : 'Meeting',
          start          : new Date(y, m, d, 10, 30),
          allDay         : false,
          backgroundColor: '#0073b7', //Blue
          borderColor    : '#0073b7' //Blue
        },
        {
          title          : 'Lunch',
          start          : new Date(y, m, d, 12, 0),
          end            : new Date(y, m, d, 14, 0),
          allDay         : false,
          backgroundColor: '#00c0ef', //Info (aqua)
          borderColor    : '#00c0ef' //Info (aqua)
        },
        {
          title          : 'Birthday Party',
          start          : new Date(y, m, d + 1, 19, 0),
          end            : new Date(y, m, d + 1, 22, 30),
          allDay         : false,
          backgroundColor: '#00a65a', //Success (green)
          borderColor    : '#00a65a' //Success (green)
        },
        {
          title          : 'Click for Google',
          start          : new Date(y, m, 28),
          end            : new Date(y, m, 29),
          url            : 'http://google.com/',
          backgroundColor: '#3c8dbc', //Primary (light-blue)
          borderColor    : '#3c8dbc' //Primary (light-blue)
        }
              ]
              ,
              editable  : true,
              droppable : true, // this allows things to be dropped onto the calendar !!!
              drop      : function(info) {
                // is the "remove after drop" checkbox checked?
                if (checkbox.checked) {
                  // if so, remove the element from the "Draggable Events" list
                  info.draggedEl.parentNode.removeChild(info.draggedEl);
                }
              },
            });

            calendar.render();
            // $('#calendar').fullCalendar()

            /* ADDING EVENTS */
            var currColor = '#3c8dbc' //Red by default
            //Color chooser button
            var colorChooser = $('#color-chooser-btn')
            $('#color-chooser > li > a').click(function (e) {
              e.preventDefault()
              //Save color
              currColor = $(this).css('color')
              //Add color effect to button
              $('#add-new-event').css({
                'background-color': currColor,
                'border-color'    : currColor
              })
            })
            $('#add-new-event').click(function (e) {
              e.preventDefault()
              //Get value and make sure it is not null
              var val = $('#new-event').val()
              if (val.length == 0) {
                return
              }

              //Create events
              var event = $('<div />')
              event.css({
                'background-color': currColor,
                'border-color'    : currColor,
                'color'           : '#fff'
              }).addClass('external-event')
              event.html(val)
              $('#external-events').prepend(event)

              //Add draggable funtionality
              ini_events(event)

              //Remove event from text input
              $('#new-event').val('')
            })
        }
    })
    </script>
@endpush