@extends('layouts.frontend.master')

@section('title', 'Login')

@section('banner')
<div class="container-fluid single-intro">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <h1 class="header-title">
                Pour Your Idea Into Coding
            </h1>
            <p class="card-title header-title">~ Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, facere.</p>
        </div>
    </div>
</div>
@endsection

@section('main-content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-10" style="margin-top: 5em">
            <h3 class="header-title text-center">Login to Posts</h3>
            <form method="POST" action="{{ route('login') }}" class="mt-5">
            @csrf
            <div class="form-group row">
                <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                <div class="col-md-6">
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} border-indigo" name="email" value="{{ old('email') }}" required autofocus>
                    @if ($errors->has('email'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
                <div class="col-md-6">
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} border-indigo" name="password" required>
                    @if ($errors->has('password'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6 offset-md-4">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }} class="custom-control-input" id="customCheck1" checked>
                        <label class="custom-control-label" for="customCheck1">Remember Me</label>
                    </div>
                </div>
            </div>
            <div class="form-group row mb-0">
                <div class="col-md-8 offset-md-4">
                    <button type="submit" class="btn btn-indigo mr-3">
                    {{ __('Login') }}
                    </button>
                    Don't have an account? click 
                    <a class="btn-link text-indigo" href="{{ route('register') }}">
                        {{ __('here.') }}
                    </a>
                </div>
            </div>
        </form>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        $('.content-wrapper').css('marginTop', '0')
        $('label').addClass('font-weight-normal')
    </script>
@endpush