@extends('layouts.frontend.master')

@section('title', $post->title)
@push('css')
    <style>
        h3:not(.blog-title) {
            font-size: 22px;
        }

        @media (max-width: 480px) {
            .title-on-single {
                font-size: 1.5rem;
            }
        }
    </style>
@endpush

@section('banner')
<div class="container-fluid single-intro">
    <div class="row justify-content-center">
        <div class="col-md-10">
            @foreach ($post->tags as $tag)
                <span class="tags tag-{{ $tag->slug }}">{{ $tag->name }}</span>
            @endforeach
            <span class="date ml-2">{{ $post->created_at->toFormattedDateString() }}</span>
            <h1 class="title-on-single">{{ $post->title }}</h1>
        </div>
    </div>
</div>
@endsection

@section('main-content')

<div class="container-fluid">
    <div class="row justify-content-center pt-5">
        <div class="col-md-1 sticky-container">
            <div class="post-shares sticky-shares ml-5 d-none d-lg-block">
                <a href="#" class="share-facebook shadow"><i class="fab fa-facebook"></i></a>
                <a href="#" class="share-twitter shadow"><i class="fab fa-twitter"></i></a>
                <a href="#" class="share-google-plus shadow"><i class="fab fa-google-plus"></i></a>
                <a href="#" class="share-pinterest shadow"><i class="fab fa-pinterest"></i></a>
                <a href="#" class="share-linkedin shadow"><i class="fab fa-linkedin"></i></a>
                <a href="#" class="share-linkedin shadow"><i class="fas fa-envelope"></i></a>
            </div>
        </div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12">
                    <div class="card mx-2">
                        <img src="{{ Storage::disk('public')->url('post/'. $post->image) }}" alt="" class="img-fluid rounded-0">
                        <div class="card-body single-blog">
                            <div class="card-tools d-lg-none d-block float-right">
                                <div class="dropdown">
                                    <button type="button" class="btn btn-tool dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="margin-top: 10px;">
                                        <i class="fas fa-share-alt"></i>
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" style="margin-left: -100px; margin-top: -5px; position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(45px, 31px, 0px);" x-placement="bottom-start">
                                        <a href="#" class="dropdown-item"><i class="fab fa-twitter"></i> Twitter</a>
                                        <a href="#" class="dropdown-item"><i class="fab fa-facebook"></i> Facebook</a>
                                        <a href="#" class="dropdown-item"><i class="fab fa-instagram"></i> Instagram</a>
                                        <a href="#" class="dropdown-item"><i class="fab fa-google-plus"></i> Google +</a>
                                        <a href="#" class="dropdown-item"><i class="fab fa-linkedin"></i> Linked in</a>
                                    </div>
                                </div>
                            </div>
                            <div class="media mt-3 mb-3">
                                <img src="{{ Storage::disk('public')->url('profile/'. $post->user->image) }}" class="mr-3 rounded-circle" alt="..." width="60">
                                <div class="media-body">
                                    <h6 class="mt-0 ml-1">{{ $post->user->name }}</h6>
                                    <small class="d-inline-block text-muted pr-1"><i class="far fa-calendar"></i> {{ $post->created_at->toFormattedDateString() }}</small>
                                    <small class="d-inline-block text-muted border-left pl-2"><i class="fas fa-eye"></i> {{ $post->view_count }}</small> 
                                </div>
                            </div>
                            <h2 class="blog-title">{{ $post->title }}</h2>

                            <!-- Ads -->
                            <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                            <ins class="adsbygoogle"
                                 style="display:block"
                                 data-ad-format="fluid"
                                 data-ad-layout-key="-5m+d1+5s-mf+hy"
                                 data-ad-client="ca-pub-9070824081326686"
                                 data-ad-slot="5334813540"></ins>
                            <script>
                                 (adsbygoogle = window.adsbygoogle || []).push({});
                            </script>

                            <!-- Content Blog -->
                            {!! $post->body !!}

                            @foreach ($post->tags as $tag)
                                <a href="{{ url('/tag/'. $tag->slug) }}" class="btn btn-sm btn-outline-primary m-1 font-weight-bold">#{{ $tag->name }}</a>
                            @endforeach

                            <!-- Ads -->
                            <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                            <ins class="adsbygoogle"
                                 style="display:block"
                                 data-ad-format="fluid"
                                 data-ad-layout-key="-5m+d1+5s-mf+hy"
                                 data-ad-client="ca-pub-9070824081326686"
                                 data-ad-slot="5334813540"></ins>
                            <script>
                                 (adsbygoogle = window.adsbygoogle || []).push({});
                            </script>

                            <!-- Prev / Next -->
                            <div class="simple-pagination mt-5">
                                @if (!empty($prev))
                                    <span class="callout border-left-0 mt-2 d-block float-left py-3" style="width: 40%; min-height: 80px;">{{ $prev->title }}</span>
                                @endif
                                
                                @if (!empty($next))
                                    <span class="callout border-left-0 mt-2 d-block float-right py-3" style="width: 40%; min-height: 80px;">{{ $next->title }}</span>
                                @endif

                                <div class="clear" style="clear: both;"></div>
                                
                                @if (!empty($prev))
                                    <a href="{{ url('/post/'. $prev->slug) }}" class="btn btn-lg btn-secondary shadow-sm rounded-25 float-left">
                                        <span aria-hidden="true">&larr;</span> Prev
                                    </a>
                                @endif
                                
                                @if (!empty($next))
                                    <a href="{{ url('/post/'. $next->slug) }}" class="btn btn-lg btn-indigo rounded-25 float-right">
                                        Next <span aria-hidden="true">&rarr;</span>
                                    </a>
                                @endif
                            </div>

                            <!-- Ads -->
                            <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                            <ins class="adsbygoogle"
                                 style="display:block"
                                 data-ad-client="ca-pub-9070824081326686"
                                 data-ad-slot="9828947544"
                                 data-ad-format="auto"
                                 data-full-width-responsive="true"></ins>
                            <script>
                                 (adsbygoogle = window.adsbygoogle || []).push({});
                            </script>

                            <!-- Comments with Discuss -->
                            <div id="disqus_thread"></div>
                            <script>
                            (function() { // DON'T EDIT BELOW THIS LINE
                            var d = document, s = d.createElement('script');
                            s.src = 'https://https-w2learn-com.disqus.com/embed.js';
                            s.setAttribute('data-timestamp', +new Date());
                            (d.head || d.body).appendChild(s);
                            })();
                            </script>
                            <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            @include('layouts.frontend.partials.widget')
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        $('blockquote').addClass('callout callout-danger')

        var $shares = $('.sticky-container .sticky-shares'),
            $sharesHeight = $shares.height(),
            $sharesTop,
            $sharesCon    = $('.sticky-container'),
            $sharesConTop,
            $sharesConleft,
            $sharesConHeight,
            $sharesConBottom,
            $offsetTop    = 80;

        function setStickyPos () {
            if ($shares.length > 0) {
                $sharesTop       = $shares.offset().top
                $sharesConTop    = $sharesCon.offset().top;
                $sharesConleft   = $sharesCon.offset().left;
                $sharesConHeight = $sharesCon.height();
                $sharesConBottom = $sharesConHeight + $sharesConTop;
            }
        }

        function stickyShares (wScroll) {
            if ($shares.length > 0) {
                if ( $sharesConBottom - $sharesHeight - $offsetTop < wScroll ) {
                    $shares.css({ position: 'absolute', top: $sharesConHeight - $sharesHeight , left:0});
                } else if ( $sharesTop < wScroll + $offsetTop ) {
                    $shares.css({ position: 'fixed', top: $offsetTop, left: $sharesConleft });
                } else {
                    $shares.css({position: 'absolute', top: 0, left: 0});
                }
            }
        }

        $(window).on('scroll', function() {
            stickyShares($(this).scrollTop());
        });

        $(window).resize(function() {
            setStickyPos();
            stickyShares($(this).scrollTop());
        });

        setStickyPos();
    </script>
@endpush