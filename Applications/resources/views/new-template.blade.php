@extends('layouts.frontend.master')

@section('title', 'Template')

@section('banner')
<div class="container-fluid single-intro">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <h1 class="header-title">
                Pour Your Idea Into Coding
            </h1>
            <p class="card-title header-title">~ Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, facere.</p>
        </div>
    </div>
</div>
@endsection

@section('main-content')
<section id="portfolio" class="clearfix">
    <div class="container">
        <header class="section-header">
            <h1 class="header-title text-center">Choose Your Favorite Template</h1>
        </header>

        <div class="row portfolio-container mt-5">
            <!-- second ads -->
            <ins class="adsbygoogle"
                 style="display:block"
                 data-ad-client="ca-pub-9070824081326686"
                 data-ad-slot="9828947544"
                 data-ad-format="auto"
                 data-full-width-responsive="true"></ins>
            <script>
                 (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
            
            <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                <div class="portfolio-wrap">
                    <img src="https://w2learn.com/assets/blog/img/portfolio/app1.jpg" class="img-fluid" alt="">
                    <div class="portfolio-info">
                        <h4><a href="#">App 1</a></h4>
                        <p>App</p>
                        <div>
                            <a href="https://w2learn.com/assets/blog/img/portfolio/app1.jpg" data-lightbox="portfolio" data-title="App 1" class="link-preview" title="Preview"><i class="fas fa-plus-circle"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 portfolio-item filter-web" data-wow-delay="0.1s">
                <div class="portfolio-wrap">
                    <img src="https://w2learn.com/assets/blog/img/portfolio/web3.jpg" class="img-fluid" alt="">
                    <div class="portfolio-info">
                        <h4><a href="#">Web 3</a></h4>
                        <p>Web</p>
                        <div>
                            <a href="https://w2learn.com/assets/blog/img/portfolio/web3.jpg" class="link-preview" data-lightbox="portfolio" data-title="Web 3" title="Preview"><i class="fas fa-plus-circle"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 portfolio-item filter-app" data-wow-delay="0.2s">
                <div class="portfolio-wrap">
                    <img src="https://w2learn.com/assets/blog/img/portfolio/app2.jpg" class="img-fluid" alt="">
                    <div class="portfolio-info">
                        <h4><a href="#">App 2</a></h4>
                        <p>App</p>
                        <div>
                            <a href="https://w2learn.com/assets/blog/img/portfolio/app2.jpg" class="link-preview" data-lightbox="portfolio" data-title="App 2" title="Preview"><i class="fas fa-plus-circle"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 portfolio-item filter-card">
                <div class="portfolio-wrap">
                    <img src="https://w2learn.com/assets/blog/img/portfolio/card2.jpg" class="img-fluid" alt="">
                    <div class="portfolio-info">
                        <h4><a href="#">Card 2</a></h4>
                        <p>Card</p>
                        <div>
                            <a href="https://w2learn.com/assets/blog/img/portfolio/card2.jpg" class="link-preview" data-lightbox="portfolio" data-title="Card 2" title="Preview"><i class="fas fa-plus-circle"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 portfolio-item filter-web" data-wow-delay="0.1s">
                <div class="portfolio-wrap">
                    <img src="https://w2learn.com/assets/blog/img/portfolio/web2.jpg" class="img-fluid" alt="">
                    <div class="portfolio-info">
                        <h4><a href="#">Web 2</a></h4>
                        <p>Web</p>
                        <div>
                            <a href="https://w2learn.com/assets/blog/img/portfolio/web2.jpg" class="link-preview" data-lightbox="portfolio" data-title="Web 2" title="Preview"><i class="fas fa-plus-circle"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 portfolio-item filter-app" data-wow-delay="0.2s">
                <div class="portfolio-wrap">
                    <img src="https://w2learn.com/assets/blog/img/portfolio/app3.jpg" class="img-fluid" alt="">
                    <div class="portfolio-info">
                        <h4><a href="#">App 3</a></h4>
                        <p>App</p>
                        <div>
                            <a href="https://w2learn.com/assets/blog/img/portfolio/app3.jpg" class="link-preview" data-lightbox="portfolio" data-title="App 3" title="Preview"><i class="fas fa-plus-circle"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 portfolio-item filter-card">
                <div class="portfolio-wrap">
                    <img src="https://w2learn.com/assets/blog/img/portfolio/card1.jpg" class="img-fluid" alt="">
                    <div class="portfolio-info">
                        <h4><a href="#">Card 1</a></h4>
                        <p>Card</p>
                        <div>
                            <a href="https://w2learn.com/assets/blog/img/portfolio/card1.jpg" class="link-preview" data-lightbox="portfolio" data-title="Card 1" title="Preview"><i class="fas fa-plus-circle"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 portfolio-item filter-card" data-wow-delay="0.1s">
                <div class="portfolio-wrap">
                    <img src="https://w2learn.com/assets/blog/img/portfolio/card3.jpg" class="img-fluid" alt="">
                    <div class="portfolio-info">
                        <h4><a href="#">Card 3</a></h4>
                        <p>Card</p>
                        <div>
                            <a href="https://w2learn.com/assets/blog/img/portfolio/card3.jpg" class="link-preview" data-lightbox="portfolio" data-title="Card 3" title="Preview"><i class="fas fa-plus-circle"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 portfolio-item filter-web" data-wow-delay="0.2s">
                <div class="portfolio-wrap">
                    <img src="https://w2learn.com/assets/blog/img/portfolio/web1.jpg" class="img-fluid" alt="">
                    <div class="portfolio-info">
                        <h4><a href="#">Web 1</a></h4>
                        <p>Web</p>
                        <div>
                            <a href="https://w2learn.com/assets/blog/img/portfolio/web1.jpg" class="link-preview" data-lightbox="portfolio" data-title="Web 1" title="Preview"><i class="fas fa-plus-circle"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('scripts')
    <script>
        $('.content-wrapper').css('marginTop', '0')
    </script>
@endpush